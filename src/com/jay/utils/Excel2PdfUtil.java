package com.jay.utils;

import com.aspose.cells.FontConfigs;
import com.aspose.cells.License;
import com.aspose.cells.SaveFormat;
import com.aspose.cells.Workbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * excel 转 pdf
 * @author 01375894
 *
 */
public class Excel2PdfUtil {
	
    public static boolean getLicense() {
        boolean result = false;
        try {
            InputStream is = Excel2PdfUtil.class.getClassLoader().getResourceAsStream("license.xml");
            License aposeLic = new License();
            aposeLic.setLicense(is);
            result = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    
    public static void excel2pdf(String excelPath, String pdfPath) {
        if (!getLicense()) {    // 验证License 若不验证则转化出的pdf文档会有水印产生
            return;
        }
        FontConfigs.setDefaultFontName("宋体");
        FontConfigs.setFontFolder("./fonts", true);  //解决linux生成pdf文件乱码问题, window下可以不需要设置字体
        FileOutputStream fileOS = null;
        try {
            File pdfFile = new File(pdfPath);
            Workbook wb = new Workbook(excelPath);
            fileOS = new FileOutputStream(pdfFile);
            wb.save(fileOS, SaveFormat.PDF);
            fileOS.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
